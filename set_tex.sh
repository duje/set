#!/bin/bash
#
# Set up BasicTeX: https://www.tug.org/mactex/BasicTeX.pdf.

italic=`tput sitm`
offitalic=`tput ritm`

echo ${bold}"Has ${italic}BasicTeX${offitalic} been installed via set_darwin.sh?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select opt in "${options[@]}"; do
  case $opt in
    yes)
      echo "Yes, got it."
      if [[ `which tlmgr` -ef "/Library/TeX/texbin/tlmgr" ]]; then
        tlmgr update --self
      fi
      if [[ `which pdflatex` -ef "/Library/TeX/texbin/pdflatex" ]]; then
        ./set_tex_packages.sh
      fi
      break
      ;;
    no)
      echo ${bold}"Select one of the following options to install BasicTeX: "${offbold}
      PS3="Select 1 or 2: "
      options=(CTAN Homebrew)
      select opt in "${options[@]}"; do
        case $opt in
          CTAN)
            echo "Chose CTAN, will curl the Comprehensive TeX Archive Network for BasicTeX and install."
            cd $HOME/Downloads
            curl -fL https://mirror.ctan.org/systems/mac/mactex/BasicTeX.pkg -o "BasicTeX.pkg"
            installer -pkg $HOME/Downloads/BasicTeX.pkg -target /usr/local/texlive/basic
            ./set_tlmgr.sh
            if [[ `which pdflatex` -ef "/Library/TeX/texbin/pdflatex" ]]; then
              ./set_tex_packages.sh
            fi
            break
            ;;
          Homebrew)
            echo "Chose Homebrew, will execute set_os set_brew() to brew install --cask BasicTeX."
            ./set_tlmgr.sh
            if [[ `which pdflatex` -ef "/Library/TeX/texbin/pdflatex" ]]; then
              ./set_tex_packages.sh
            fi
            break
            ;;
        esac
      done
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done
