#!/bin/bash
#
# Set up Firefox Add-ons.

bold=`tput bold`
offbold=`tput sgr0`

url='https://addons.mozilla.org/firefox/downloads/file/'
readonly mozilla_extensions_path='Library/Application\ Support/Mozilla/Extensions/'

declare -a addons
addons=("3995753/adblock_for_firefox" "3984258/firefox_translations" "4002797/i_dont_care_about_cookies" "3926466/tridactly_vim")
file_extension=".xpi"
version="-latest"

for addon in "${addons[@]}"; do
  open "${url}${addon}${version}${file_extension}"
done

echo ${bold}"Do you want to wget Firefox Add-ons?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
      cd mozilla_extensions_path
      wget \
        https://addons.mozilla.org/firefox/downloads/file/3995753/adblock_for_firefox-latest.xpi \
        https://addons.mozilla.org/firefox/downloads/file/3984258/firefox_translations-latest.xpi \
        https://addons.mozilla.org/firefox/downloads/file/4002797/i_dont_care_about_cookies-latest.xpi \
        https://addons.mozilla.org/firefox/downloads/file/3926466/tridactyl_vim-latest.xpi
      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done
