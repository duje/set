#!/bin/bash
#
# Set up TeX Live package manager: https://tug.org/texlive/tlmgr.html.

bold=`tput bold`
offbold=`tput sgr0`

# https://tug.org/texlive/quickinstall.html
echo ${bold}"Do you want to install the TeX Live package manager (tlmgr)?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select opt in "${options[@]}"; do
  case $opt in
    yes)
      echo "Change directory to TeX Live"
      cd /usr/local/texlive/2021basic
      echo "Install Tex Live"
      if ! [ -x `command -v perl` ]; then
        echo "Perl could not be found, will curl"
        curl -L http://xrl.us/installperlosx | bash
        echo "Continue Tex Live installation"
        perl install-tl --gui=text
      else
        wget https://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
        chmod u+x install-tl
        perl install-tl --gui=text
      fi
      break
      ;;
    no)
      echo "Got it, will terminate.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

echo ${bold}"Do you want to reset the default location where tlmgr looks for updates?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select opt in "${options[@]}"; do
  case $opt in
    yes)
      # tlmgr: https://www.tug.org/texlive/doc/tlmgr.html, tlmgr option repository ctan
      tlmgr option repository http://mirror.ctan.org/systems/texlive/telnet
      break
      ;;
    no)
      echo "Got it, will terminate.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

echo ${bold}"Do you want to curl http://mirror.ctan.org/systems/texlive/tlnet/update-tlmgr-latest.sh?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select opt in "${options[@]}"; do
  case $opt in
    yes)
      curl -fL http://mirror.ctan.org/systems/texlive/tlnet/update-tlmgr-latest.sh -o "update-tlmgr-latest.sh"
      chmod u+x update-tlmgr-latest.sh
      break
      ;;
    no)
      echo "Got it, will terminate.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

echo ${bold}"Do you want to execute update-tlmgr-latest.sh?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select opt in "${options[@]}"; do
  case $opt in
    yes)
      ./update-tlmgr-latest.sh
      break
      ;;
    no)
      echo "Got it, will terminate.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done
