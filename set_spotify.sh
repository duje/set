#!/bin/bash
#
# Set up Spotify: https://github.com/Rigellute/spotify-tui.

bold=`tput bold`
offbold=`tput sgr0`

italic=`tput sitm`
offitalic=`tput ritm`

echo ${bold}"Do you want to build ${italic}spotify-tui${offitalic} from source?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
      cargo install spotify-tui
      break
      ;;
    no)
      echo ${bold}"Do you want to install ${italic}spotify-tui${offitalic} with snap?"${offbold}
      PS3="Select 1 or 2: "
      options=(yes no)
      select option in "${options[@]}"; do
        case $option in
          yes)
            snap install spt
            break
            ;;
          no)
            echo "Got it, terminated.";
            break
            ;;
          *)
            echo "Invalid, try another of the two given options.";
            continue
            ;;
        esac
      done
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

echo ${bold}"Do you want add host entries to /etc/hosts to block Spotify ads?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
      echo "$(cat hosts-spotify.txt)" | sudo tee -a /etc/hosts
      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done
