#!/bin/bash
#
# Set up Vim: https://github.com/vim/vim.

bold=`tput bold`
offbold=`tput sgr0`

italic=`tput sitm`
offitalic=`tput ritm`

echo ${bold}"Do you want compile Vim from source?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select opt in "${options[@]}"; do
  case $opt in
    yes)
      cd $HOME
      git clone https://github.com/vim/vim.git
      cd vim/src
      sudo make distclean
      ./configure
      \ --with-features=huge
      \ --enable-luainterp
      \ --enable-perlinterp
      \ --enable-python3interp
      make
      sudo make install
      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another option.";
      continue
      ;;
  esac
done


echo ${bold}"Do you want curl ${italic}Vim Plug${offitalic}?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select opt in "${options[@]}"; do
  case $opt in
    yes)
      curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another option.";
      continue
      ;;
  esac
done
