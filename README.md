## Set

Set is a collection of Bash executables aiming to simplify setting up macOS. Run [set_darwin.sh](set_darwin.sh).
