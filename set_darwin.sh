#!/bin/bash
#
# Set up macOS.

readonly uname=`uname -s`

bold=`tput bold`
offbold=`tput sgr0`

italic=`tput sitm`
offitalic=`tput ritm`

line=`tput smul`
noline=`tput rmul`

if [ "$(uname)" == "Darwin" ]; then
  set_brew()
  {
    echo ${bold}"Do you want to set Homebrew?"${offbold}
    PS3="Select 1 or 2: "
    options=(yes no)
    select option in "${options[@]}"; do
      case $option in
        yes)
          `which -s brew`
          if [[ $? != 0 ]]; then
            echo "curl Homebrew"
            /bin/bash -c `curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh`
          else
            echo "Update Homebrew"
            brew upgrade
          fi

          echo ${bold}"Do you want to set Homebrew in ${italic}.zprofile${offitalic}?"${offbold}
          PS3="Select 1 or 2: "
          options=(yes no)
          select option in "${options[@]}"; do
            case $option in
              yes)
                user=`whoami`
                echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> /Users/"${user}"/.zprofile
                eval "$(/opt/homebrew/bin/brew shellenv)"
                continue
                ;;
              no)
                echo "Got it, terminated.";
                break
                ;;
              *)
                echo "Invalid, try another of the two given options.";
                continue
                ;;
            esac
          done

          declare -a formulae
          formulae=(ansible antigen aspell bat calcurse chafa cmake code-minimap coursier delve deno dolt doxygen dwm evince exa gh ghex git git-lfs gnupg go hashcat highlight htop httrack hugo ii inkscape john jp2a jq kubectl mas libmagic lazygit libsixel lnav meson neomutt neovim nerdctl newsboat node openconnect pandoc pass perl poppler postgresql@13 prettier pyenv python3 qemu qpdf ranger ripgrep rclone rsync rust snap spotify spotifyd stow task terraform terragrunt telnet tmux torsocks upx vault weechat wget wireguard-tools w3m xsel yarn zsh zsh-history-substring-search)

          casks=(alacritty balenaetcher basictex bitwarden blender clockify docker drawio element firefox fontforge freecad ghidra gimp insomnia jitsi kap libreoffice lmms magicavoxel metasploit natron protonvpn satellite-eyes sloth threema thunderbird)
          declare -a casks

          brews=`brew ls`
          for brew in "${brews[@]}"; do
            for formula in "${formulae[@]}"; do
              if [[ ! brew = formula ]]; then
                brew install "${formula}"
              else
                brew upgrade "${formula}"
              fi
            done
          done
          for brew in "${brews[@]}"; do
            for cask in "${casks[@]}"; do
              if [[ ! brew = cask ]]; then
                brew install --cask "${cask}"
              else
                brew upgrade --cask "${cask}"
              fi
            done
          done
          continue
          ;;
        no)
          echo "Got it, terminated.";
          break
          ;;
        *)
          echo "Invalid, try another of the two given options.";
          continue
          ;;
      esac
    done
  }

  set_defaults()
  {
    echo ${bold}"Do you want to set the ${line}defaults${noline} for macOS?"${offbold}
    PS3="Select 1 or 2: "
    options=(yes no)
    select option in "${options[@]}"; do
      case $option in
        yes)
          ./set_darwin_defaults.sh
          break
          ;;
        no)
          echo "Got it, terminated.";
          break
          ;;
        *)
          echo "Invalid, try another of the two given options.";
          continue
          ;;
      esac
    done
  }

  set_firefox()
  {
    echo ${bold}"Do you want to set Firefox Add-ons?"${offbold}
    PS3="Select 1 or 2: "
    options=(yes no)
    select option in "${options[@]}"; do
      case $option in
        yes)
          ./set_firefox.sh
          break
          ;;
        no)
          echo "Got it, terminated.";
          break
          ;;
        *)
          echo "Invalid, try another of the two given options.";
          continue
          ;;
      esac
    done
  }

  set_fonts()
  {
    echo ${bold}"Do you want to download additional ${line}fonts${noline}?"${offbold}
    PS3="Select 1 or 2: "
    options=(yes no)
    select option in "${options[@]}"; do
      case $option in
        yes)
          ./set_fonts.sh
          break
          ;;
        no)
          echo "Got it, terminated.";
          break
          ;;
        *)
          echo "Invalid, try another of the two given options.";
          continue
          ;;
      esac
    done
  }

  set_nvim()
  {
   git clone --depth 1 https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim
  }

  set_python()
  {
    echo "Set Python"
    python -m pip --version
    pip install --user pipenv sourcery-cli
  }

  set_spotify()
  {
    echo ${bold}"Do you want to set ${italic}Spotify${offitalic}?"${offbold}
    PS3="Select 1 or 2: "
    options=(yes no)
    select option in "${options[@]}"; do
      case $option in
        yes)
          ./set_spotify.sh
          break
          ;;
        no)
          echo "Got it, terminated.";
          break
          ;;
        *)
          echo "Invalid, try another of the two given options.";
          continue
          ;;
      esac
    done
  }

  set_tex()
  {
    echo ${bold}"Do you want to set ${italic}TeX Live${offitalic}?"${offbold}
    PS3="Select 1 or 2: "
    options=(yes no)
    select option in "${options[@]}"; do
      case $option in
        yes)
          ./set_tex.sh
          break
          ;;
        no)
          echo "Got it, terminated.";
          break
          ;;
        *)
          echo "Invalid, try another of the two given options.";
          continue
          ;;
      esac
    done
  }

  set_tor()
  {
    echo "wget Tor"
    wget https://dist.torproject.org/torbrowser/12.0a3/TorBrowser-12.0a3-osx64_en-US.dmg
  }

  set_vim()
  {
    echo ${bold}"Do you want to set VIM?"${offbold}
    PS3="Select 1 or 2: "
    options=(yes no)
    select option in "${options[@]}"; do
      case $option in
        yes)
          ./set_vim.sh
          echo "Skip use of .viminfo"
          vim -i viminfo NONE
          break
          ;;
        no)
          echo "Got it, terminated.";
          break
          ;;
        *)
          echo "Invalid, try another of the two given options.";
          continue
          ;;
      esac
    done
  }

  clean_brew()
  {
    echo "Clean Homebrew"
    brew cleanup -s
    rm -rfv `brew --cache`
    brew tap --repair
    brew cleanup
  }

  clean_docker()
  {
    echo "Prune Docker objects"
    docker container stop `docker ps -aq` >/dev/null 2>&1
    docker container rm `docker ps -aq` >/dev/null 2>&1
    docker system prune -af
  }

  clean_os()
  {
    echo "Clear logs"
    rm -rfv /private/var/logs &>/dev/null
    rm -rfv /Library/Logs &>/dev/null
    rm -rfv /Library/Caches &>/dev/null
    rm -rfv ~/Library/Developer/Xcode/DerivedData/* &>/dev/null
    rm -rfv ~/Library/Developer/Xcode/Archive/* &>/dev/null

    echo "Purge disk cache"
    purge
  }

  update_mas()
  {
    echo "Update App Store"
    mas upgrade
  }

  update_os()
  {
    echo "Update OS"
    softwareupdate --list --install --all
  }
fi

case $* in
  all)
    set_brew
    set_defaults
    set_firefox
    set_fonts
    set_nvim
    set_python
    set_spotify
    set_tex
    set_tor
    set_vim
    clean_brew
    clean_docker
    clean_os
    update_mas
    update_os
    ;;
  clean)
    clean_brew
    clean_docker
    clean_os
    ;;
  set)
    set_brew
    set_defaults
    set_firefox
    set_fonts
    set_nvim
    set_python
    set_spotify
    set_tex
    set_tor
    set_vim
    ;;
  update)
    update_mas
    update_os
    ;;
  *)
    echo "Use of $0: {all|clean|set|update}"
    exit 1
    ;;
esac
