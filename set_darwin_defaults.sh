#!/bin/bash
#
# Manage macOS defaults.

bold=`tput bold`
offbold=`tput sgr0`

italic=`tput sitm`
offitalic=`tput ritm`

echo ${bold}"Do you want to enable the debug menu in ${italic}Disk Utility.app${offitalic}?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
      defaults write com.apple.DiskUtility DUDebugMenuEnabled -bool true
      defaults write com.apple.DiskUtility advanced-image-options -bool true
      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

echo ${bold}"Do you want to set ${italic}Dock & Menu Bar${offitalic}?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
      echo "Enable Airport, Battery, Bluetooth, Clock"
      for domain in ~/Library/Preferences/ByHost/com.apple.systemuiserver.*; do
      defaults write com.apple.systemuiserver menuExtras -array \
        "/System/Library/CoreServices/Menu Extras/AirPort.menu" \
        "/System/Library/CoreServices/Menu Extras/Battery.menu" \
        "/System/Library/CoreServices/Menu Extras/Bluetooth.menu" \
        "/System/Library/CoreServices/Menu Extras/Clock.menu"
      done
      defaults write "${domain}" dontAutoLoad -array \
          "/System/Library/CoreServices/Menu Extras/User.menu" \
          "/System/Library/CoreServices/Menu Extras/TimeMachine.menu" \
          "/System/Library/CoreServices/Menu Extras/Volume.menu"

      echo "Disable Notification Center and remove Menu Bar icon"
      launchctl unload -w /System/Library/LaunchAgens/com.apple.notificationcenterui.plist 2> /dev/null

      echo "Automatically hide and show Dock"
      defaults write com.apple.dock autohide -bool true

      echo "Remove delay of Dock & Menu Bar display"
      defaults write com.apple.dock autohide-delay -float 0

      echo "Show indicator lights for open applications in Dock"
      defaults write com.apple.dock show-process-indicators -bool true

      echo "Disable animation on opening applications in Dock & Menu Bar"
      defaults write com.apple.dock launchanim -bool false

      echo "Enable iTunes track notifications in Dock & Menu Bar"
      defaults write com.apple.dock itunes-notifications -bool true

      echo "Bottom Dock & Menu Bar"
      defaults write com.apple.dock "Orientation" -string "bottom"

      echo "Show only active applications"
      defaults write com.apple.dock static-only -bool true

      echo "Highlight hidden applications in Dock & Menu Bar"
      defaults write com.apple.dock showhidden -bool true

      echo "Dock & Menu Bar minimize animation effect"
      defaults write com.apple.dock "mineffect" -string "scale"

      echo "Disable animatation when opening applications from Dock"
      defaults write com.apple.dock launchanim -bool false

      echo "Wipe default app icons from Dock"
      defaults write com.apple.dock persistent-apps -array

      echo "Define Dock & Menu Bar icon size"
      defaults write com.apple.dock -string tilesize -int 45

      echo "Show remaining battery time and percentage"
      defaults write com.apple.menuextra.battery ShowPercent -bool true
      defaults write com.apple.menuextra.battery ShowTime -bool true

      echo "Enable Dock & Menu Bar transparency"
      defaults write NSGlobalDomain AppleEnableMenuBarTransparency -bool true

      echo "Disable system-wide resume"
      defaults write com.apple.systempreferences NSQuitAlwaysKeepsWindows -bool false

      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

echo ${bold}"Do you want to configure ${italic}Finder.app${offitalic}?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
    printf "${italic}Finder.app${offitalic}\n"
    echo "Set default Finder window"
    defaults write com.finder -array \
      '{"ComputerViewSettings" = {' \
      '{"CustomViewStyleVersion" = 1;' \
      '{"WindowState" = {' \
      '{"ContainerShowSidebar" = 0;' \
      '{"ShowSidebar" = 0;' \
      '{"ShowStatusBar" = 1;' \
      '{"ShowTabView" = 0;' \
      '{"ShowToolbar" = 1; };};' \
      '{"FavoriteTagNames" = ("");};'
      '{"ICloudViewSettings" = {' \
      '{"WindowState" = {' \
      '{"ContainerShowSidebar" = 0;' \
      '{"ShowSidebar" = 0;' \
      '{"ShowStatusBar" = 1;' \
      '{"ShowTabView" = 0;' \
      '{"ShowToolbar" = 1; };};'
      '{"NetworkViewSettings" = {' \
      '{"WindowState" = {' \
      '{"ContainerShowSidebar" = 0;' \
      '{"ShowSidebar" = 0;' \
      '{"ShowStatusBar" = 1;' \
      '{"ShowTabView" = 0;' \
      '{"ShowToolbar" = 1; };};'
      '{"SearchRecentsViewSettings" = {' \
      '{"WindowState" = {' \
      '{"ContainerShowSidebar" = 0;' \
      '{"ShowSidebar" = 0;' \
      '{"ShowStatusBar" = 1;' \
      '{"ShowTabView" = 0;' \
      '{"ShowToolbar" = 1; };};'
      '{"SearchViewSettings" = {' \
      '{"WindowState" = {' \
      '{"ContainerShowSidebar" = 0;' \
      '{"ShowSidebar" = 0;' \
      '{"ShowStatusBar" = 1;' \
      '{"ShowTabView" = 0;' \
      '{"ShowToolbar" = 1; };};' \
      '{"ShowExternalHardDrivesOnDesktop" = 1;}' \
      '{"ShowHardDrivesOnDesktop" = 0;}' \
      '{"ShowPathbar" = 1;}' \
      '{"ShowRecentTags" = 0;}' \
      '{"ShowRemovableMediaOnDesktop" = 0;}' \
      '{"ShowSidebar" = 0;}' \
      '{"ShowStatusBar" = 1;}' \
      '{"TrashViewSettings" = {' \
      '{"CustomViewStyleVersion" = 1;' \
      '{"WindowState" = {' \
      '{"ContainerShowSidebar" = 0;' \
      '{"ShowSidebar" = 0;' \
      '{"ShowStatusBar" = 1;' \
      '{"ShowTabView" = 0;' \
      '{"ShowToolbar" = 1; };};'

      echo "Show hidden files"
      defaults write com.apple.finder AppleShowAllFiles -bool true

      echo "Set Finder sidebar icon size"
      defaults write NSGlobalDomain NSTableViewDefaultSizeMode -int 1

      echo "Show hidden files by default"
      defaults write com.apple.finder AppleShowAllFiles -bool true

      echo "Empty Trash securely by default"
      defaults write com.apple.finder EmptyTrashSecurely -bool true

      echo "Allow cmd+Q to quit finder"
      defaults write com.apple.finder QuitMenuItem -bool true

      echo "Disable animations on OS"
      defaults write com.apple.finder DisableAllAnimations -bool true

      echo "Default location for new finder window is root"
      defaults write com.apple.finder NewWindowTarget -string "PfCm"

      echo "Show path in Finder"
      defaults write com.apple.finder ShowPathbar -bool true

      echo "Show status bar in finder"
      defaults write com.apple.finder ShowStatusBar -bool true

      echo "Disable display of icons for hard drives, servers and removable media on Desktop"
      defaults write com.apple.finder ShowExternalHardDrivesOnDesktop -bool false
      defaults write com.apple.finder ShowHardDrivesOnDesktop -bool false
      defaults write com.apple.finder ShowMountedServersOnDesktop -bool false
      defaults write com.apple.finder ShowRemovableMediaOnDesktop -bool false

      echo "Automatically open Finder when volume is mounted"
      defaults write com.apple.frameworks.diskimages auto-open-ro-root -bool true
      defaults write com.apple.frameworks.diskimages auto-open-rw-root -bool true
      defaults write com.apple.finder OpenWindowForNewRemovableDisk -bool true

      echo "Disable warning when changing file extension"
      defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false

      echo "Display POSIX path as Finder window title"
      defaults write com.apple.finder FXShowPosixPathInTitle -bool true

      echo "Column view for Finder"
      defaults write com.apple.finder FXPreferredViewStyle -string "clmv"

      echo "Disable warning before emptying Trash"
      defaults write com.apple.finder WarnOnEmptyTrash -bool false

      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

echo ${bold}"Do you want to configure ${italic}Mail.app${offitalic}?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
      echo "Set email address to 'foo@example.com' instead of 'Foo Bar <foo@example.com>'"
      defaults write com.apple.mail AddressesIncludeNameOnPasteboard -bool false

      echo "Disable spell checking"
      defaults write com.apple.mail SpellCheckingBehavior -string "NoSpellCheckingEnabled"
      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

echo ${bold}"Do you want to set up ${italic}Messages.app${offitalic}?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
      echo "Disable automatic emoji substitution"
      defaults write com.apple.messageshelper.MessageController SOInputLineSettings -dict-add "automaticEmojiSubstitutionEnablediMessage" -bool false

      echo "Disable spell checking"
      defaults write com.apple.messageshelper.MessageController SOInputLineSettings -dict-add "continuousSpellCheckingEnabled" -bool false
      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

echo ${bold}"Do you want to set up ${italic}Photos.app${offitalic}?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
      echo "Prevent Photos from opening when device is plugged in"
      defaults -currentHost write com.apple.ImageCapture disableHotPlug -bool true
      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

echo ${bold}"Do you want to set up ${italic}QuickTimes Player.app${offitalic}?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
      echo "Auto-play videos when opened with QuickTime Player.app"
      defaults write com.apple.QuickTimePlayerX MGPlayMovieOnOpen -bool true
      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

echo ${bold}"Do you want to set the name of the computer? (y/n)"${offbold}
read -r reponse
if [[ $response =~ ^([yY][eE][sS])$ ]]; then
  echo "What would you like the computer to be named?"
  read name
  echo "Are you sure that \"$name\" is your choice? (y/n)"
  read -r response
  if [[ $response2 =~ ^([yY][eE][sS])$ ]]; then
    scutil --set ComputerName $name
    scutil --set HostName $name
    scutil --set LocalHostName $name
    defaults write /Library/Preferences/SystemConfiguration/com.apple.smb.server NetBIOSName $name
    echo "You haven chosen \"$name\"."
  else
    echo "What would other name would you like to give?"
    read -r name
    scutil --set ComputerName $name
    scutil --set HostName $name
    scutil --set LocalHostName $name
    defaults write /Library/Preferences/SystemConfiguration/com.apple.smb.server NetBIOSName $name
    echo "You haven chosen \"$name\"."
  fi
fi

echo ${bold}"Do you want to configure NSGlobalDomain?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
      echo "Disable sound effect on boot"
      nvram SystemAudioVolume=" "

      echo "Restart automatically if computer freezes"
      systemsetup -setrestartfreeze on

      echo "Set timezone; sudo systemsetup -listtimezones for other values"
      systemsetup -settimezone "GMT" > /dev/null

      echo "Disable guest account"
      defaults write com.apple.MCX DisableGuestAccount -bool true

      echo "Enable airport -s"
      ln -s /System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport /usr/local/bin/airport

      # https://support.apple.com/en-us/HT208209
      echo "Avoid creation of .DS_Store on network volumes"
      defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
      defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true

      echo "Expand save panel by default"
      defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true
      defaults write NSGlobalDomain PMPrintingExpandedStateForPrint -bool true
      defaults write NSGlobalDomain PMPrintingExpandedStateForPrint2 -bool true

      echo "Set language and text formats"
      defaults write NSGlobalDomain AppleLanguages -array "en" "de"
      defaults write NSGlobalDomain AppleLocale -string "en_US@currency=EUR"
      defaults write NSGlobalDomain AppleMeasurementUnits -string "Centimeters"
      defaults write NSGlobalDomain AppleMetricUnits -bool true

      echo "Enable full keyboard access for all controls"
      defaults write NSGlobalDomain AppleKeyboardUIMode -int 3

      echo "Disable press-and-hold for keys in favor of key repeat"
      defaults write NSGlobalDomain ApplePressAndHoldEnabled -bool false

      echo "Set fast keyboard repeat rate"
      defaults write NSGlobalDomain KeyRepeat -int 0.03

      echo "Set a short delay until key repeat"
      defaults write NSGlobalDomain InitialKeyRepeat -int 15

      echo "Show file extensions"
      defaults write NSGlobalDomain AppleShowAllExtensions -bool true

      echo "Always show scrollbar"
      defaults write NSGlobalDomain AppleShowScrollBars -string "Always"

      echo "Disable auto-correct"
      defaults write NSGlobalDomain NSAutomaticSpellingCorrectionEnabled -bool false

      echo "Disable opening and closing window animations"
      defaults write NSGlobalDomain NSAutomaticWindowAnimationsEnabled -bool false

      echo "Default file save location is disk"
      defaults write NSGlobalDomain NSDocumentSaveNewDocumentsToCloud -bool false

      echo "Disable dash and quote substitution"
      defaults write NSGlobalDomain NSAutomaticDashSubstitutionEnabled -bool false
      defaults write NSGlobalDomain NSAutomaticQuoteSubstitutionEnabled -bool false

      echo "Expand save panel by default"
      defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true
      defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode2 -bool true

      echo "Expand print panel by default"
      defaults write NSGlobalDomain PMPrintingExpandedStateForPrint -bool true
      defaults write NSGlobalDomain PMPrintingExpandedStateForPrint2 -bool true
      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

echo ${bold}"Do you want to set up ${italic}Safari.app${offitalic}?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
      echo "Disable AutoFill"
      defaults write com.apple.Safari AutoFillFromAddressBook -bool false
      defaults write com.apple.Safari AutoFillPasswords -bool false
      defaults write com.apple.Safari AutoFillCreditCardData -bool false
      defaults write com.apple.Safari AutoFillMiscellaneousForms -bool false

      echo "Disable thumbnail cache for History and Top Sites"
      defaults write com.apple.Safari DebugSnapshotsUpdatePolicy -int 2

      echo "Show blank start page"
      defaults write com.apple.Safari HomePage -string "about:blank"

      echo "Enable debug menu"
      defaults write com.apple.Safari IncludeDevelopMenu -bool true
      defaults write com.apple.Safari IncludeInternalDebugMenu -bool true

      echo "Disable queries to Apple"
      defaults write com.apple.Safari UniversalSearchEnabled -bool false
      defaults write com.apple.Safari SuppressSearchSuggestions -bool true

      echo "Do Not Track"
      defaults write com.apple.Safari SendDoNotTrackHTTPHeader -bool true

      echo "Show Favorites bar"
      defaults write com.apple.Safari ShowFavoritesBar -bool true

      echo "Show full URL in address bar"
      defaults write com.apple.Safari ShowFullURLInSmartSearchField -bool true

      echo "Hide sidebar in Top Sites"
      defaults write com.apple.Safari ShowSidebarInTopSites -bool false

      echo "Warn about fraudulent websites"
      defaults write com.apple.Safari WarnAboutFraudulentWebsites -bool true

      echo "Block pop-ups"
      defaults write com.apple.Safari WebKitJavaScriptCanOpenWindowsAutomatically -bool false
      defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2JavaScriptCanOpenWindowsAutomatically -bool false
      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

echo ${bold}"Do you want to set Desktop & Screen Saver?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
      echo "Disable auto-adjusting brightness"
      defaults write /Library/Preferences/com.apple.iokit.AmbientLightSensor "Automatic Display Enabled" -bool false
      defaults write /Library/Preferences/com.apple.iokit.AmbientLightSensor "Automatic Keyboard Enabled" -bool false

      echo "Enable transparency"
      defaults write com.apple.universalaccess reduceTransparency -bool false

      echo "Require password immediately after sleep or screen saver begins"
      defaults write com.apple.screensaver askForPassword -int 1
      defaults write com.apple.screensaver askForPasswordDelay -int 0

      echo "Enable AdminHostInfo and HostName at login"
      defaults write /Library/Preferences/com.apple.loginwindow AdminHostInfo HostName
      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done


echo ${bold}"Do you want to set up ${italic}Screenshot.app${offitalic}?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
      printf "${italic}Screenshot.app${offitalic}\n"
      echo "Set default location of screenshot to ~/Documents"
      defaults write com.apple.screencapture location ${HOME}/Documents

      echo "Set default screenshot type to PNG"
      defaults write com.apple.screencapture type -string "PNG" # else PDF
      # curl -o ~/Library/Preferences/com.apple.Terminal.plist http://dujes.net && defaults read com.appleTerminal

      echo "Disable shadow in Screenshot.app"
      defaults write com.apple.screencapture disable-shadow -bool true
      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

echo ${bold}"Do you want to change default indexing and search in Spotlight? (y/n)"${offbold}
read -r reponse
if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]; then
# https://www.apple.com/privacy/labels/
defaults write com.apple.spotlight orderedItems -array \
  '{"enabled" = 1;"name" = "APPLICATIONS";}' \
  '{"enabled" = 0;"name" = "MENU_SPOTLIGHT_SUGGESTIONS";}' \
  '{"enabled" = 0;"name" = "MENU_CONVERSION";}' \
  '{"enabled" = 0;"name" = "MENU_EXPRESSION";}' \
  '{"enabled" = 1;"name" = "MENU_DEFINITION";}' \
  '{"enabled" = 1;"name" = "SYSTEM_PREFS";}' \
  '{"enabled" = 0;"name" = "DOCUMENTS";}' \
  '{"enabled" = 1;"name" = "DIRECTORIES";}' \
  '{"enabled" = 0;"name" = "SPREADSHEETS";}' \
  '{"enabled" = 0;"name" = "PRESENTATIONS";}' \
  '{"enabled" = 1;"name" = "PDF";}' \
  '{"enabled" = 0;"name" = "MESSAGES";}' \
  '{"enabled" = 0;"name" = "CONTACT";}' \
  '{"enabled" = 0;"name" = "EVENT_TODO";}' \
  '{"enabled" = 0;"name" = "IMAGES";}' \
  '{"enabled" = 0;"name" = "BOOKMARKS";}' \
  '{"enabled" = 0;"name" = "MUSIC";}' \
  '{"enabled" = 0;"name" = "MOVIES";}' \
  '{"enabled" = 1;"name" = "FONTS";}' \
  '{"enabled" = 0;"name" = "MENU_OTHER";}'

# metadata server
killall mds > /dev/null 2>&1
# index status for /
mdutil -i on / > /dev/null
# erase rebuild local store for /
mdutil -E / > /dev/null
fi

echo ${bold}"Do you want to set up ${italic}App Store.app${offitalic}?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
      echo "Enable WebKit Developer Tools in Mac App Store"
      defaults write com.apple.appstore WebKitDeveloperExtras -bool true

      echo "Enable Debug Menu in App Store"
      defaults write com.apple.appstore ShowDebugMenu -bool true

      echo "Enable update check"
      defaults write com.apple.SoftwareUpdate AutomaticCheckEnabled -bool true

      echo "Check for software updates daily"
      defaults write com.apple.SoftwareUpdate ScheduleFrequency -int 1

      echo "Disable dowload updates in background"
      defaults write com.apple.SoftwareUpdate AutomaticDownload -int 0

      echo "Install system data files and security updates"
      defaults write com.apple.SoftwareUpdate CriticalUpdateInstall -int 1

      echo "Disable downloadable apps purchased on another Mac"
      defaults write com.apple.SoftwareUpdate ConfigDataInstall -int 0

      echo "Turn on app auto-update"
      defaults write com.apple.commerce AutoUpdate -bool true

      echo "Disallow App Store to reboot machine on updates"
      defaults write com.apple.commerce AutoUpdateRestartRequired -bool false
      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

echo ${bold}"Do you want to set ${italic}Terminal${offitalic}?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
      echo "Enabling UTF-8 only in Terminal and set Homebrew as theme"
      defaults write com.apple.terminal StringEncodings -array 4
      defaults write com.apple.Terminal -string "Default Window Settings" -string "Homebrew"
      defaults write com.apple.Terminal -string "Startup Window Settings" -string "Homebrew"
      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

echo ${bold}"Do you want to set up ${italic}TextEdit.app${offitalic}?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
      echo "Use plain text mode"
      defaults write com.apple.TextEdit RichText -int 0

      echo "Open and save files as UTF-8 in TextEdit"
      defaults write com.apple.TextEdit PlainTextEncoding -int 4
      defaults write com.apple.TextEdit PlainTextEncodingForWrite -int 4
      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

echo ${bold}"Do you want to set up ${italic}Time Machine.app${offitalic}?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
      echo "Prevent Time Machine from prompting to use new hard drives as backup volume"
      defaults write com.apple.TimeMachine DoNotOfferNewDisksForBackup -bool true

      echo "Disable local Time Machine backups"
      hash tmutil &> /dev/null && sudo tmutil disablelocal

      echo "Do not offer backup for new disks"
      defaults write com.apple.TimeMachine DoNotOfferNewDisksForBackup -bool true

      echo "Set menubar digital clock format"
      defaults write com.apple.menuextra.clock DateFormat -string "\"EEE d MMM HH:mm:ss\""
      break
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

  echo "Disallow .cache"
  echo "[default]" >> /etc/nsmb.conf 
  echo "dir_cache_max_cnt=0" >> /etc/nsmb.conf
  exit
}

echo ${bold}"Do you want to set up ${italic}Trackpad.app{offitalic}?$"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select option in "${options[@]}"; do
  case $option in
    yes)
      com.apple.driver.AppleBluetoothMultitouch.trackpad -array \
        '{"Clicking" = 1;}' \
        '{"DragLock" = 0;}' \
        '{"Dragging" = 0;}' \
        '{"TrackpadCornerSecondaryClick" = 0;}' \
        '{"TrackpadFiveFingerPinchGesture" = 2;}' \
        '{"TrackpadFourFingerHorizSwipeGesture" = 2;}' \
        '{"TrackpadFourFingerPinchGesture" = 2;}' \
        '{"TrackpadFourFingerVertSwipeGesture" = 2;}' \
        '{"TrackpadHandResting" = 1;}' \
        '{"TrackpadHorizScroll" = 1;}' \
        '{"TrackpadMomentumScroll" = 1;}' \
        '{"TrackpadPinch" = 1;}' \
        '{"TrackpadRightClick" = 1;}' \
        '{"TrackpadRotate" = 1;}' \
        '{"TrackpadScroll" = 1;}' \
        '{"TrackpadThreeFingerDrag" = 1;}' \
        '{"TrackpadThreeFingerHorizSwipeGesture" = 0;}' \
        '{"TrackpadThreeFingerTapGesture" = 0;}' \
        '{"TrackpadThreeFingerVertSwipeGesture" = 0;}' \
        '{"TrackpadTwoFingerDoubleTapGesture" = 1;}' \
        '{"TrackpadTwoFingerFromRightEdgeSwipeGesture" = 3;}' \
        '{"USBMouseStopsTrackpad" = 0;}' \
        '{"UserPreferences" = 1;}' \
        '{"version" = 5;}'

      # echo "Enable Trackpad tap to click"
      # defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
      continue
      ;;
    no)
      echo "Got it, terminated.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done

declare -a apps
apps=('Activity Monitor' 'Address Book' 'Calendar' 'Contacts' 'Dock' 'Finder' 'Mail' 'Messages' 'Photos' 'Safari' 'SystemUIServer')
echo "Kill affected applications"
for app in "${apps[@]}"; do
  killall "$app" >/dev/null 2>&1
done
