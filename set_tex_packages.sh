#!/bin/bash
#
# Install packages from Comprehensive TeX Archive Network (CTAN): https://www.ctan.org/.

bold=`tput bold`
offbold=`tput sgr0`

# https://tug.org/texlive/quickinstall.html
echo ${bold}"Do you install TeX packages?"${offbold}
PS3="Select 1 or 2: "
options=(yes no)
select opt in "${options[@]}"; do
  case $opt in
    yes)
      declare -a packages
      packages=(currvita ebgaramond fontaxes hyphenat latexmk marginnote sectsty)
      for package in "${packages[@]}"; do
        tlmgr install $package #reinstall --with-doc --with-src
        echo "$package installed"
      done
      break
      ;;
    no)
      echo "Got it, will terminate.";
      break
      ;;
    *)
      echo "Invalid, try another of the two given options.";
      continue
      ;;
  esac
done
