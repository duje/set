#!/bin/bash
#
# Curl desired fonts.

readonly unname=`uname -s`

if [ "$(uname)" == "Darwin" ]; then
  cd $HOME/Library/Fonts
  echo "curl NotoSans, IBMPlex Sans and Serif, JetBrainsMono and Nerd Fonts Roboto Mono, Iosevka Term and JetBrains Mono"
  curl -fLo NotoSans-Bold.ttf https://github.com/google/fonts/raw/main/ofl/notosans/NotoSans-Bold.ttf  \
    https://github.com/google/fonts/raw/main/ofl/notosans/NotoSans-Italic.ttf -o "NotoSans-Italic.ttf" \
    https://github.com/google/fonts/raw/main/ofl/notosans/NotoSans-Regular.ttf -o "NotoSans-Regular.ttf" \
    https://github.com/IBM/plex/raw/master/IBM-Plex-Sans/fonts/complete/otf/IBMPlexSans-Bold.otf -o "IBMPlexSans-Bold.otf" \
    https://github.com/IBM/plex/raw/master/IBM-Plex-Sans/fonts/complete/otf/IBMPlexSans-Italic.otf -o "IBMPlexSans-Italic.otf" \
    https://github.com/IBM/plex/raw/master/IBM-Plex-Sans/fonts/complete/otf/IBMPlexSans-Regular.otf -o "IBMPlexSans-Regular.otf" \
    https://github.com/IBM/plex/raw/master/IBM-Plex-Serif/fonts/complete/otf/IBMPlexSerif-Bold.otf -o "IBMPlexSerif-Bold.otf" \
    https://github.com/IBM/plex/raw/master/IBM-Plex-Serif/fonts/complete/otf/IBMPlexSerif-Italic.otf -o "IBMPlexSerif-Italic.otf" \
    https://github.com/IBM/plex/raw/master/IBM-Plex-Serif/fonts/complete/otf/IBMPlexSerif-Regular.otf -o "IBMPlexSerif-Regular.otf" \
    https://github.com/JetBrains/JetBrainsMono/raw/master/fonts/otf/JetBrainsMono-ExtraLight.otf -o "JetBrainsMono-ExtraLight.otf" \
    https://github.com/JetBrains/JetBrainsMono/raw/master/fonts/otf/JetBrainsMono-ExtraLightItalic.otf -o "JetBrainsMono-ExtraLightItalic.otf" \
    https://github.com/JetBrains/JetBrainsMono/raw/master/fonts/otf/JetBrainsMono-Light.otf -o "JetBrainsMono-Light.otf" \
    https://github.com/JetBrains/JetBrainsMono/raw/master/fonts/otf/JetBrainsMono-Regular.otf -o "JetBrainsMono-Regular.otf" \
    https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/Iosevka/Light/complete/Iosevka%20Term%20Light%20Nerd%20Font%20Complete%20Mono.ttf -o "Iosevka-Term-Light-Nerd-Font-Complete-Mono.ttf" \
    https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/Iosevka/Light-Italic/complete/Iosevka%20Term%20Light%20Italic%20Nerd%20Font%20Complete%20Mono.ttf -o "Iosevka-Term-Light-Italic-Nerd-Font-Complete-Mono.ttf" \
    https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/Iosevka/Regular/complete/Iosevka%20Term%20Nerd%20Font%20Complete%20Mono.ttf -o "Iosevka-Term-Nerd-Font-Complete-Mono.ttf" \
    https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/JetBrainsMono/NoLigatures/ExtraLight/complete/JetBrains%20Mono%20NL%20ExtraLight%20Nerd%20Font%20Complete%20Mono.ttf -o "JetBrains-Mono-NL-ExtraLight-Nerd-Font-Complete-Mono.ttf" \
    https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/JetBrainsMono/NoLigatures/ExtraLightItalic/complete/JetBrains%20Mono%20NL%20ExtraLight%20Italic%20Nerd%20Font%20Complete%20Mono.ttf -o "JetBrains-Mono-NL-ExtraLight-Italic-Nerd-Font-Complete-Mono.ttf" \
    https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/JetBrainsMono/NoLigatures/Light/complete/JetBrains%20Mono%20NL%20Light%20Nerd%20Font%20Complete%20Mono.ttf -o "JetBrains-Mono-NL-Light-Nerd-Font-Complete-Mono.ttf" \
    https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/JetBrainsMono/NoLigatures/LightItalic/complete/JetBrains%20Mono%20NL%20Light%20Italic%20Nerd%20Font%20Complete%20Mono.ttf -o "JetBrains-Mono-NL-Light-Italic-Nerd-Font-Complete-Mono.ttf" \
    https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/RobotoMono/Bold/complete/Roboto%20Mono%20Bold%20Nerd%20Font%20Complete.ttf -o "Roboto-Mono-Bold-Nerd-Font-Complete.ttf" \
    https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/RobotoMono/Bold-Italic/complete/Roboto%20Mono%20Bold%20Italic%20Nerd%20Font%20Complete.ttf -o "Roboto-Mono-Bold-Italic-Nerd-Font-Complete.ttf" \
    https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/RobotoMono/Italic/complete/Roboto%20Mono%20Italic%20Nerd%20Font%20Complete.ttf -o "Roboto-Mono-Italic-Nerd-Font-Complete.ttf" \
    https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/RobotoMono/Light-Italic/complete/Roboto%20Mono%20Light%20Italic%20Nerd%20Font%20Complete.ttf -o "Roboto-Mono-Light-Italic-Nerd-Font-Complete.ttf" \
    https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/RobotoMono/Regular/complete/Roboto%20Mono%20Nerd%20Font%20Complete.ttf -o "Roboto-Mono-Nerd-Font-Complete.ttf"
fi
